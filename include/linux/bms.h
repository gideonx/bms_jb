/* include/linux/bms.h
 *
 *  Copyright (c) 2012 morfic
 *  Adapted for BMS by GideonX
 *
 * Trinity aggregated includes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#ifndef __LINUX_BMS_H
#define __LINUX_BMS_H

#define BMS_BOOT_FREQ 1836000
static int user_policy_max_freq = BMS_BOOT_FREQ;

#endif /* __LINUX_BMS_H */
